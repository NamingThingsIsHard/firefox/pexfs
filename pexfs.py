#!/usr/bin/env python3
"""
pexfs
Copyright (C) 2021 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import argparse
import json
import operator
import os
import re
import shlex
import signal
import subprocess
from argparse import ArgumentParser
from io import FileIO
from os.path import commonpath
from pathlib import Path

import sys

# How much data we will accept to pin
MAX_MB = 10
MAX_BYTES = MAX_MB * 1024 * 1024

# How long to wait for the first data to come in
READ_TIMEOUT = 5
IPFS_TIMEOUT = 30

ADDON_REGEX = r'^[\w@\.]+$'
VERSION_REGEX = r'^v?\d+\.\d+\.\d+[a-z]*$'


def main(addon_id: str, version: str, parent_folder: str, file: FileIO = None):
    # Create parent directories
    parent = Path(parent_folder).resolve()
    parent.mkdir(exist_ok=True, parents=True)

    addon_folder = (parent / addon_id).resolve()
    if commonpath([parent, addon_folder]) != str(parent):
        _print("Invalid addon path")
        exit(1)

    # Where the files are kept and the first thing to hash
    files_folder = addon_folder / "files"
    files_folder.mkdir(exist_ok=True, parents=True)

    # Read and store data from stdin
    _print("Pinning incoming data")
    _print("Max %s MB allowed" % MAX_MB)
    version_file = (files_folder / build_filename(version)).resolve()
    if commonpath([version_file, files_folder]) != str(files_folder):
        _print("Invalid files path")
        exit(1)

    if file:
        version_file.write_bytes(file.buffer.read())
    else:
        version_file.write_bytes(read_stdin())

    # Hash the files folder
    _print("Add xpi to IPFS")
    files_folder_hash = ipfs_add(files_folder)

    # Regenerate update manifest
    update_manifest_file = addon_folder / "update.json"
    update_manifest = update_json(addon_id, files_folder, files_folder_hash)
    update_manifest_file.write_text(json.dumps(update_manifest))

    # Rehash the addon folder
    _print("Add JSON and folder to IPFS")
    print(ipfs_add(addon_folder))


def _print(string):
    """
    Prints to stderr in order to allow callers to debug.
    """
    print(string, file=sys.stderr)


def build_filename(version):
    return f"{version}.xpi"


def read_stdin() -> bytes:
    data = None

    # Timeout reading from stdin
    def cancel_read(*_):
        if data is not None:
            return
        _print("No data provided after %s" % READ_TIMEOUT)
        exit(2)

    signal.signal(signal.SIGALRM, cancel_read)
    signal.alarm(READ_TIMEOUT)

    length = 0
    all_data = b''
    data = sys.stdin.buffer.read(1024)
    while data:
        all_data += data
        length += len(data)
        if length > MAX_BYTES:
            _print("Reached limit")
            exit(1)

        data = sys.stdin.buffer.read(1024)
    return all_data


def ipfs_add(files_folder: Path):
    completed = subprocess.run([
        "ipfs",
        "add",  # Add pin and hash
        "--recursive",  # to add folders
        "-Q",  # Output only a hash
        str(files_folder),
    ], stdout=subprocess.PIPE, stderr=subprocess.PIPE,
                               timeout=IPFS_TIMEOUT)
    if completed.returncode != 0:
        _print("Couldn't add files to IPFS")
        _print(completed.stdout.decode())
        _print(completed.stderr.decode())
        exit(1)

    return completed.stdout.decode().strip()


def update_json(
        addon_id: str,
        files_folder: Path,
        dir_hash: str,
):
    """
    https://extensionworkshop.com/documentation/manage/updating-your-extension/#enable-update
    """
    xpi_files = sorted([
        xpi
        for xpi in files_folder.iterdir()
        if xpi.is_file()
    ], key=operator.attrgetter("name"))
    return {
        addon_id: {
            "updates": [
                {
                    "version": os.path.splitext(xpi_file.name)[0],
                    "update_link": f"http://ipfs.io/ipfs/{dir_hash}/{xpi_file.name}",
                }
                for xpi_file in xpi_files
            ]
        }
    }


class RegexType:
    def __init__(self, regex, error_message=None):
        self.regex = regex
        self.error_message = error_message or f"Invalid argument didn't pass regex '{self.regex}'"

    def __call__(self, value):
        if not re.match(self.regex, value):
            raise argparse.ArgumentTypeError(f"{value} didn't match regex '{self.regex}'")
        return value


def add_common_final_args(parser: ArgumentParser):
    parser.add_argument("addon_id", type=RegexType(ADDON_REGEX))
    parser.add_argument("version", type=RegexType(
        regex=VERSION_REGEX,
        error_message="Invalid version string. Should look like <major>.<minor>.<patch>"
                      f" or v<major>.<minor>.<patch> or this regex '{VERSION_REGEX}'"
    ))


if __name__ == '__main__':
    parser = ArgumentParser(
        description="Publishes consecutive versions of a web-extension to a directory on IPFS."
                    " A json is generated/updated and the directory's hash is printed"
    )
    parser.add_argument(
        "-p", "--parent",
        help="Parent folder where the data will be stored",
        required=True,
    )

    # Take the final arguments from SSH_ORIGINAL_COMMAND or from commandline
    # SSH_ORIGINAL_COMMAND is set by SSH when a command is executed by a public key
    #  in authorized_keys
    ssh_argv = os.environ.get('SSH_ORIGINAL_COMMAND')
    if ssh_argv:
        args = parser.parse_args()

        ssh_parser = ArgumentParser()
        add_common_final_args(ssh_parser)
        ssh_args = ssh_parser.parse_args(shlex.split(ssh_argv))

        # common args
        parent_dir = args.parent
        addon_id = ssh_args.addon_id
        version = ssh_args.version
        file = None
    else:
        parser.add_argument(
            "-f", "--file",
            help="File to add",
            required=True,
            type=argparse.FileType('r')
        )
        add_common_final_args(parser)
        args = parser.parse_args()

        # common args
        parent_dir = args.parent
        addon_id = args.addon_id
        version = args.version
        file = args.file

    main(addon_id, version, parent_dir, file)
